Bonjour !!!

Ce README vous permettra d'installer une machine virtuelle avec comme système d'exploitation Linux

Lien vers mon GitLab : https://gitlab.com/DanielMalleron/sae-install

### 1/ Préparation de l'Installation

Souvent l'option virtualisation et désactivé par défault cette option permet a votre processeur de fonctionner sur 
plusieurs système exploitation en même temps,on gagnera en performance et on pourra éviter des problèmes

Pour cela nous devons accéder a votre bios regarder sur internet comment y accéder car cela change celon votre processeur
Ensuite suivait les consignes selon votre processeur: https://support.bluestacks.com/hc/fr-fr/articles/115003174386-Comment-puis-je-activer-la-virtualisation-VT-sur-mon-PC-


### 2/ Installation de la machine virtuelle "Virtual Box" 

Allez sur leur site : https://www.virtualbox.org/
Puis installer la version qui vous correspond le mieux.


### 3/ Installation de l'iso d'un système d'exploitation.

Allez sur le site du système d'exploitation que vous voulez puis installer l'iso 
!!! Atention prenait bien le fichier qui se termine par .iso !!!

### 4/ Configuration de virtualbox

##### A/Crée la machine

  Lancer virtualbox puis crée une nouvelle machine 
  Choisissez type linux version Ubuntu-64bits (Mais vous pouvez choisir ce que vous voulez tant que votre iso correspond)
  Après choisissez vos paramètres selon vos besoin 

##### B/Crée un disque dur

  Sur virtualbox cliquer sur Configuration puis dans stockage cliquer sur le logo "ajouter un disque dur"
  Puis choisissez la taille que vous voulez 

##### C/Mettre l'iso

  Toujours dans stockage cliquer sur le logo "ajouter un lecteur"
  Puis choisissez votre iso que vous avez installé

### 5/ Preparation et lancement de la machine 

N'oubliez pas d'augmenter la capacité de ram et la capacité de processeur que virtualbox peut utiliser
Lancer votre machine 
Puis faites vos configuration (Heure , langue ...)



Maintenant que vous avez votre machine virtuelle, nous allons installer tout les outils nécessaire pour un environnement de travail.

### Python :

```
sudo apt install python3
```

Pour tester :
```
python3
``` 
Puis 
```
print("Hello World!")
```

### Pytest : 

```
sudo apt-get install python3-pytest
```

### Docker :

```
sudo apt install docker
```


### Java : 

```
sudo apt install default-jdk 
```

```
sudo apt install default-jre 
```

ou 

```
https://www.java.com/fr/download/help/linux_install.html 
```


### extension Vs Code :

Extension Gitlab Workflow

```
code --install-extension Gitlab.gitlab-workflow  
```       

Extension python

```
code --install-extension ms-python.python 
```       

```
 code --install-extension ms-python.vscode-pylance
```

Extension pour une Docstring auto

```
code --install-extension njpwerner.autodocstring  
```  



### Gitlab 

```
sudo apt install git
```

### Gitlab relié a Vs code: 

-Etape 1 crée un token

-Etape 2 installer l'extensions "Gitlab workflow" sur vs code  : code --install-extension Gitlab.gitlab-workflow

-Etape 3 view -> command palette -> gitlab : add account to vs code 

-Etape 4 entrez votre token 









